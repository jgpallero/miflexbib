.. -*- coding: utf-8 -*-

.. Este fichero está formateado con reStructuredText para que se vea en HTML en
.. https://bitbucket.org/jgpallero/plantilla-libro-latex/. Se ha procurado que
.. siga siendo legible en formato de texto plano.

********************************************************************************
FICHERO README DE miflexbib
********************************************************************************

Modificación del paquete de gestión de bibliografías para LaTeX ``flexbib``. Se
añade soporte para la detección en castellano de situaciones en las que es
necesario cambiar la conjunción copulativa ``y`` por ``e`` en listados de
autores y/o editores.

..------------------------------------------------------------------------------
.. kate: encoding utf-8; end-of-line unix;
.. kate: syntax restructuredtext; indent-mode cstyle;
.. kate: replace-tabs on; space-indent on; tab-indents off; indent-width 4;
.. kate: line-numbers on; folding-markers on; remove-trailing-space on;
.. kate: backspace-indents on; show-tabs on;
.. kate: word-wrap-column 80; word-wrap-marker-color #D2D2D2; word-wrap off;
