\documentclass[11pt,a4paper]{article}
\usepackage[spanish]{babel}
\usepackage{lmodern}
\usepackage{hologo}
\usepackage{anysize}
\marginsize{2.5cm}{2.5cm}{2.5cm}{2.5cm}
\usepackage{hyperref}
\hypersetup{colorlinks,citecolor=blue,linkcolor=blue,urlcolor=blue}
\usepackage{doi}
\renewcommand{\doitext}{DOI:~}
\usepackage[spanish,authoryear,round,colon,datebegin]{config/flexbib/flexbib}
% \renewcommand{\bibnamefont}[1]{#1}
\renewcommand{\bibfnamefont}[1]{#1}
\renewcommand{\bibetalfont}[1]{#1}
\renewcommand{\bibandfont}[1]{#1}
\renewcommand{\citenamefont}[1]{#1}
\renewcommand{\citetalfont}[1]{#1}
\renewcommand{\citeandfont}[1]{#1}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
\title{Modificaciones sobre \texttt{flexbib} para su mejor adaptaci\'on a la
       escritura de bibliograf\'ias en castellano}
\author{Jos\'e Luis Garc\'ia Pallero}
\date{Versi\'on 1.0\\25 de junio de 2017}
\maketitle

\section{Introducci\'on}

A la hora de gestionar en castellano bibliograf\'ias con \hologo{BibTeX} uno de
los mejores paquetes, en mi opini\'on, es
\texttt{flexbib}\footnote{\url{www.latex.um.es/retazos/leccion_15/leccion_15.htm}.}.
Basado en gran medida \citep{mira2004} en el paquete \texttt{natbib},
\texttt{flexbib} permite un grado de personalizaci\'on bastante alto de la
apariencia de las citas y referencias, adem\'as de la particularizaci\'on de
varios elementos a determinados idiomas (por el momento est\'an soportados el
castellano, el franc\'es y el ingl\'es, aunque la extensi\'on a otros es muy
sencilla).

Pero a la vez presenta algunos inconvenientes que hacen su uso inc\'omodo en
algunas circunstancias. El primero de ellos es que el paquete no est\'a incluido
en CTAN\footnote{\url{www.ctan.org}.} ni en ninguna distribuci\'on de \LaTeX{}
corriente, por lo que la compilaci\'on de un documento en una computadora
distinta a donde fue originalmente escrito se complica.

Otro punto d\'ebil, \'este en lo que a la gesti\'on de bibliograf\'ias en idioma
espa\~nol se refiere, es que no provee la capacidad de hacer el cambio de la
forma <<y>>  por <<e>> de la conjunci\'on copulativa en los listados de autores
y/o editores cuando as\'i se requiere. La modificaci\'on de \texttt{flexbib}
aqu\'i presentada\footnote{\url{https://bitbucket.org/jgpallero/miflexbib}.}
soluciona (creo) este inconveniente.

\section{Uso local de \texttt{flexbib}}

Debido al hecho indicado de que \texttt{flexbib} no se incluye en las
distribuciones est\'andar de \LaTeX{} se propone un uso local del paquete, no en
el sentido de instalarlo en nuestra m\'aquina de trabajo, sino en el de
adjuntarlo al documento concreto que estemos escribiendo. Para ello se han
organizado los ficheros de los que consta el paquete en una carpeta llamada
\texttt{config/flexbib}, la cual ha de ser incluida en el directorio ra\'iz (en
el mismo nivel que el fichero \texttt{*.tex} que contenga la orden
\verb|\documentclass|) de nuestro proyecto de trabajo. Dicha carpeta ha de tener
obligatoriamente el nombre indicado, ya que esa ruta est\'a contenida en algunas
l\'ineas del fichero \texttt{config/flexbib/flexbib.sty}.

A\~nadido el directorio a nuestro proyecto, el uso de \texttt{flexbib} se indica
mediante la orden
\begin{verbatim}
\usepackage[opciones...]{config/flexbib/flexbib}
\end{verbatim}
en el pre\'ambulo de nuestro documento, donde hay que especificar la ruta del
fichero de estilo. Las opciones que tiene esta versi\'on son las mismas que las
del paquete original, descritas en \cite{mira2004}. La llamada a
\verb|\bibliographystyle| ha de hacerse tambi\'en especificando la ruta del
fichero de estilo:
\begin{verbatim}
\bibliographystyle{config/flexbib/flexbib}
\end{verbatim}

De este modo, siempre que con nuestro proyecto llevemos el directorio
\texttt{config/flexbib} con sus ficheros podremos compilar el documento en
cualquier computadora que disponga de una distribuci\'on de \LaTeX{} instalada.

Dos detalles a tener en cuenta:
\begin{itemize}
\item La inclusi\'on del paquete con \verb|\usepackage| de esta versi\'on de
      \texttt{flexbib} ha de hacerse \textbf{siempre despu\'es} de la del
      paquete \texttt{doi} (si se va a usar), ya que internamente se redefine
      una variable declarada por \'este. Si el paquete \texttt{doi} no se usa,
      este identificador aparecer\'a en la bibliograf\'ia como texto normal, sin
      hiperv\'inculo asociado.
\item Si se va a usar el campo \texttt{url} en la base de datos bibliogr\'afica
      el paquete \texttt{hyperref} (o cualquier otro que defina el comando
      \verb|\url|) \textbf{ha de ser cargado} obligatoriamente.
\end{itemize}

\section{Lista de autores y/o editores}

En castellano se da un cambio de la conjunci\'on copulativa <<y>> a <<e>> cuando
en una enumeraci\'on el \'ultimo elemento comienza con el fonema \texttt{/i/} y
no forma parte de un diptongo. En una bibliograf\'ia nos encontramos con
enumeraciones en las listas de autores y editores, por lo que si el apellido o
el nombre (depende de la ordenaci\'on utilizada) as\'i lo exige la conjunci\'on
ha de ser cambiada, a ser posible autom\'aticamente.

\citet[p\'ags. 30 y 31]{bezos2014} indica que en las listas de autores el
\'ultimo puede ser separado del resto por el mismo car\'acter que el utilizado
para distinguir los otros elementos de la enumeraci\'on, o bien utilizar otro
tipo de s\'imbolo como el \&, por ejemplo, desapareciendo as\'i el problema del
cambio de <<y>> por <<e>>. Sin embargo, el uso de la conjunci\'on para la
separaci\'on del \'ultimo autor sigue siendo muy com\'un.

El sistema \texttt{flexbib} no proporciona ninguna funcionalidad para el cambio
<<y>>-<<e>> cuando sea necesario, por lo que se ha modificado su c\'odigo fuente
para incorporarla. A grandes rasgos, y sin tener en cuenta casos especiales, que
ser\'an detallados m\'as adelante, los cambios que realiza la variante propuesta
en este documento son los siguientes:
\begin{itemize}
\item Cambio de la part\'icula <<y>> por <<e>>, tanto en la bibliograf\'ia como
      en las citas, cuando el apellido del \'ultimo autor comienza por <<I>>,
      <<\'I>>, <<Hi>>, <<H\'i>> o <<Y>>.
\item Cambio de la part\'icula <<y>> por <<e>> en la bibliograf\'ia cuando el
      \textbf{nombre} del \'ultimo \textbf{editor} de los tipos de referencia
      \texttt{InProceedings} e \texttt{InCollection} comienza por <<I>>,
      <<\'I>>, <<Hi>>, <<H\'i>> o <<Y>>.
\end{itemize}

Ahora bien, hay casos especiales en los que aunque la \'ultima palabra de una
enumeraci\'on empiece por <<I>>, <<\'I>>, <<Hi>>, <<H\'i>> o <<Y>> no se ha de
hacer el cambio <<y>>-<<e>>, a saber:
\begin{itemize}
\item Que la primera s\'ilaba sea un diptongo, como por ejemplo en <<hierro>>.
\item Un apellido cuya primera letra sea una <<Y>> que no funcione como vocal,
      como <<Yebra>>, por ejemplo.
\item Un apellido en un idioma distinto al castellano, que aunque comience por
      <<I>>, <<\'I>>, <<Hi>>, <<H\'i>> o <<Y>> tenga una pronunciaci\'on
      distinta a la del fonema \texttt{/i/}.
\end{itemize}

Por \'ultimo, y siguiendo con las palabras en otros idiomas distintos al
castellano, cuando el \'ultimo t\'ermino de una enumeraci\'on comience por el
fonema \texttt{/i/}, aunque la(s) letra(s) inicial(es) sea(n) distinta(s) de
<<I>>, <<\'I>>, <<Hi>>, <<H\'i>> o <<Y>> tambi\'en es necesario el cambio de
<<y>> a <<e>>, ya que prevalece el criterio fon\'etico sobre el
gr\'afico\footnote{\url{http://www.fundeu.es/recomendacion/y-hielo-no-e-hielo-1232/}.}.

Ante la dificultad de programar todos los casos particulares
posibles\footnote{Las modificaciones en el c\'odigo fuente sobre el paquete
\texttt{flexbib} original relativas a este apartado se han hecho sobre el
fichero \texttt{config/flexbib/flexbib.bst}.} se ha optado por a\~nadir dos
nuevos campos a las definiciones de referencias en los archivos \texttt{*.bib},
los cuales permiten manejar las excepciones al comportamiento general. Estos
campos son:
\begin{itemize}
\item \texttt{chand}, de \textit{change and}, el cual puede almacenar dos
      valores:
      \begin{itemize}
      \item \texttt{yes}, que fuerza el cambio <<y>>-<<e>> aunque seg\'un las
            reglas generales no haya que hacerlo. Esta opci\'on es \'util para
            los casos de nombres en otros idiomas que empiecen por el fonema
            \texttt{/i/} aunque la(s) letra(s) inicial(es) sea(n) distinta(s) de
            <<I>>, <<\'I>>, <<Hi>>, <<H\'i>> o <<Y>>.
      \item \texttt{no}, que impide el cambio <<y>>-<<e>> aunque la(s) letra(s)
            inicial(es) sea(n) igual(es) a <<I>>, <<\'I>>, <<Hi>>, <<H\'i>> o
            <<Y>>. Esta opci\'on es \'util para tener en cuenta los casos
            especiales de diptongos y similares.
      \end{itemize}
\item \texttt{chanded}, de \textit{change and-editors}, el cual puede almacenar
      los mismos valores que el campo \texttt{chand} y sirve para lo mismo que
      \'este, pero en lo referente al campo \texttt{editor} de los tipos de
      referencia \texttt{InProceedings} e \texttt{InCollection}.
\end{itemize}
Cualquier otro valor pasado a \texttt{chand} y \texttt{chanded} no es tenido en
cuenta (as\'i como la omisi\'on de uno o ambos campos), por lo que los cambios
<<y>>-<<e>> se realizan siguiendo los criterios generales de inicio de palabra
por <<I>>, <<\'I>>, <<Hi>>, <<H\'i>> o <<Y>>.

\section{Ejemplos de referencias}

\begin{verbatim}
@Article{alvarez2002,
    title = {C{\'o}mo encender fuego},
    author = {J. {{\'A}lvarez} and P. {Higham} and L. {Manrique} and
              J. {{\'I}bar}},
    journal = {Journal of Cooking},
    volume = {37},
    number = {2},
    year = {2002},
    pages = {222-521},
    doi = {10.1111/j.1745-6584.2001.tb02363.x}
}
\end{verbatim}

\begin{verbatim}
@Article{alvarez2011,
    title = {C{\'o}mo cocinar verdura},
    author = {Juan {{\'A}lvarez} and Ralph {Eaton}},
    journal = {Journal of Cooking},
    volume = {46},
    number = {1},
    year = {2011},
    pages = {220-257},
    doi = {10.1190/1.1444893},
    chand = {yes}
}
\end{verbatim}

\begin{verbatim}
@InCollection{casas2010,
    title = {Andando se llega a todas partes},
    booktitle = {C{\'o}mo viajar por el mundo},
    author = {M. {Casas} and Gabriel {Yebra}},
    editor = {Mariano {Casas} and Federico {V{\'a}zquez} and Hirt {Jackson}},
    publisher = {R{\'u}a ediciones},
    address = {Madrid},
    year = {2010},
    isbn = {84-87461-93-X},
    chand = {no},
    chanded = {no}
}
\end{verbatim}

\section{Citas de ejemplo}

Adem\'as del c\'odigo de la modificaci\'on de \texttt{flexbib} que se puede
encontrar en \url{https://bitbucket.org/jgpallero/miflexbib} tambi\'en se
proporciona una base de datos de test llamada \texttt{biblio/biblio.bib}, en la
que se recogen varios ejemplos inventados de casos particulares que se pueden
dar en bibliograf\'ias en castellano. A continuaci\'on se citan todas ellas y al
final del documento aparecen las referencias: \cite{alvarez2011,alvarez2010,
alvarez2009,alvarez2008,alvarez2007,alvarez2006,alvarez2005,alvarez2004,
alvarez2003,alvarez2002,alvarez2001,burgueno1978,burgueno1977,casas2010,
casas2009,diaz2001,diaz2000,diaz1999,diaz1998,garcia2017,hernandez2003,
hernandez2007,suarez2001}.

\bibliographystyle{config/flexbib/flexbib}
\bibliography{biblio/biblio}
\nocite{alvarez2011,alvarez2010,alvarez2009,alvarez2008,alvarez2007,alvarez2006,
        alvarez2005,alvarez2004,alvarez2003,alvarez2002,alvarez2001,
        burgueno1978,burgueno1977,casas2010,casas2009,diaz2001,diaz2000,
        diaz1999,diaz1998,garcia2017,hernandez2003,hernandez2007,suarez2001}
\end{document}
